## Table of Contents

- [Table of Contents](#table-of-contents)
- [Description](#description)
- [Setup](#setup)
  - [Setup Requirements](#setup-requirements)
  - [Beginning with tcg\_debuild](#beginning-with-tcg_debuild)
- [Usage](#usage)
  - [Pbuilder parameters](#pbuilder-parameters)
  - [Chroots](#chroots)
  - [Environment variables](#environment-variables)
  - [Result directory structure](#result-directory-structure)
  - [Building packages](#building-packages)
  - [Force chroot rebuilds](#force-chroot-rebuilds)
- [Limitations](#limitations)
- [To do](#to-do)
- [References](#references)


## Description

Module creates a Debian package build server utilizing `pbuilder` with a `cowbuilder` backend. It creates `pbuilder` chroots for any number of Ubuntu and Debian releases facilitating package creation.

Features of this module in comparison with [su_debuild](https://code.stanford.edu/puppetpublic/su_debuild) module:

* Entire configuration resides in Hiera.
* Uses debootstrap features to import keys into the chroots.
* Use hooks to import extra repos in chroots.
* An option to embed repos into chroots or use them at the
  build time only. Also works for direct logins in chroots
  and script execution within chroots.
* Automatic download of keys and keyrings from http sources.
* Use a single customizeable pbuilder configuration. No need to create
  custom pbuilderrc files for different distros or user accounts.
  The customization is done using a few variables in user's shell environment. 
* Support for local repositories based on user's build results.
* Builds can be run against Debian suite names like `stable`, `unstable`, etc.
* User customizeable option to automatically open chroot shell on failed builds.
* User customizeable option to automatically sign packages.
* Obsoletes pbuild script in favor or pdebuild direct invocation.
* Automatic rebuild of chroots when relevant configuration changes.
* An option to delete chroots.

## Setup

### Setup Requirements

The module uses:

* [puppetlabs-apt](https://forge.puppet.com/modules/puppetlabs/apt)
* [puppet-stdlib](https://forge.puppet.com/modules/puppetlabs/stdlib)
* [puppet-cron](https://forge.puppet.com/modules/puppet/cron),
* [saz-sudo](https://forge.puppet.com/modules/saz/sudo)
* [puppet-archive](https://forge.puppet.com/modules/puppet/archive).

Working configuration of the build server is designed to reside in the node's data source in hiera. A [complete working example](examples/server.stanford.edu.yaml) is provided in the examples directory.

### Beginning with tcg_debuild

Once the configuration is completed in hiera, just include the module it in the servers' manifest:

```
include tcg_debuild
```

## Usage

The build server configuration in hiera consists of a few pbuilder parameters and a chroot hash describing the properties of the chroots to be created.

### Pbuilder parameters

* `cowbuild_group` - a group having the rights to execute cowbuilder under elevated privileges. Defaults to `all`.
* `basepath` - a path to pbuilder cache, which includes chroots themselves, apt cache, hooks, build space, etc. Defaults to `/var/cache/pbuilder`.
* `keyring_dir` - a directory to store the keyrings, defaults to `/usr/share/keyrings`.

### Chroots

`chroots` parameter describes the pbuilder base chroots to be created. Here is an example of one:

```yaml
tcg_debuild::chroots:
  jammy:
    ensure: present
    distribution: ubuntu
    mirror:
      repo_name: ubuntu_mirror
      location: 'http://debian.stanford.edu/ubuntu'
      key: ubuntu-archive-keyring.gpg
      components: main multiverse restricted universe
      optional: false
      sources: false
    repos:
      - repo_name: ubuntu_security
        location: 'http://debian.stanford.edu/ubuntu'
        components: main multiverse restricted universe
        release_suffix: security
        key: ubuntu-archive-keyring.gpg
        optional: false
        sources: false
      - repo_name: ubuntu_updates
        location: 'http://debian.stanford.edu/ubuntu'
        components: main multiverse restricted universe
        release_suffix: updates
        optional: false
        key: ubuntu-archive-keyring.gpg
        sources: false
      - repo_name: ubuntu_backports
        location: 'http://debian.stanford.edu/ubuntu'
        key: ubuntu-archive-keyring.gpg
        components: main multiverse restricted universe
        release_suffix: backports
        optional: false
        sources: false
      - repo_name: stanford
        location: 'http://debian.stanford.edu/debian-stanford'
        release: bookworm
        components: main contrib non-free
        key: stanford-keyring.gpg
        optional: false
        sources: false
      - repo_name: stanford_local
        location: 'http://debian.stanford.edu/debian-local'
        components: main contrib non-free
        key: stanford-keyring.gpg
        optional: false
        sources: false
      - repo_name: nginx
        location: 'http://nginx.org/packages/ubuntu'
        key: 'https://nginx.org/keys/nginx_signing.key'
        components: nginx
        optional: true
        sources: true
    packages:
      - aptitude
      - debconf-utils
      - debhelper
      - gnupg
      - module-assistant
      - apt-utils
  bullseye:
    ensure: present
    ...<further description of bullseye, more chroots>...
```

Explicit specifications of all repos for each chroot can be pretty tedious. Using YAML anchors, aliases and overrides as in the example configuration coming with the module, can make configuration easy and DRY (Don't Repeat Yourself):

```yaml
tcg_debuild::chroots:
  jammy:
    ensure: present
    distribution: ubuntu
    mirror: *ubuntu_mirror
    repos:
      - *ubuntu_security
      - *ubuntu_updates
      - *ubuntu_backports
      - <<: *stanford
        release: bookworm
      - <<: *stanford_local
        release: bookworm
      - *nginx
    packages: *common_packages
  bullseye:
    ensure: present
    ...<further description of bullseye, more chroots>...
```

Each chroot has the following parameters:

* hash keys - OS release code names. Will be used to create and name coresponding chroots as `base-<release>.cow` in the path defined by `basepath` parameter.
* `ensure` - if *present*, create base chroot, otherwise delete it. Defaults to *present*.
* `distribution` - Linux distribution. At the moment can be only *debian* or *ubuntu*. Defaults to *debian*.
* `suite` - Debian suite, like *unstable*, *testing*, *stable*, *oldstable*.
* `mirror` - description of the main distribution repository.
* `repos` - array with descriptions of each additional repository.
* `packages` - array of additional packages names to be installed in the chroot.

Both signle `mirror` hash and array of repo hashes use the same repo type, which you can see in the [REFERENCE.md](REFERENCE.md). Here are the parameters:

* `repo_name` - the name of repository. Can be anything you want. Used to generate a name of the file placed in */etc/apt/sources.list.d* as well as variable in pbuilder configuration for *optional* repos. For instance, if you name a repo *private_repo*, then its source list file would be named *private_repo.list* and, if a repo is optional, its variable would be named *PRIVATE_REPO*. The name must be in a format of a valid bash environment variable, i.e. consist only of alphanumeric characters and underscores.
* `location` - repository URL.
* `priority` - priority given to an entire repository. Useful for prioritizing backports, etc.
* `release` - release name in the debian source line. Defaults to the name of OS release, which chroot is created for. For instance, in the chroot definition for *jammy* this would be the default value of the release. Sometimes you may want to specify release explicitly, like *bookwrom* in conjunction with a *location* of Stanford Debian repository, even though the chroot may be for *jammy* (see an example above).
* `release_suffix` - Optional method of adding a suffix to a default release. For instance, if you are describing a backports repository for *bullseye* chroot, you can set the suffix to *backports*, which would result in a release name *bullseye-backports*. Of course, instead of specifying suffix, you can specify an entire release name *bullsye-backports* in the *release* parameter. Either way works, but using suffixes allows to avoid frequent yaml overrides.
* `key` - key for the repository. Can be specified as a file name of a key *on the host system* or http(s) URL, where a key may be downloaded in a format of a keybox (`.gpg` extension) or text format key representation. In the latter case the key would be converted to a keyring and stored on the build system. In the former just placed on the host system as-is. The directory to store the keys is defined by a parameter `keyring_dir`.
* `sources` - Boolean telling whether to include source packages together with binary.
* `optional` - If *false*, the repo source list file would be created in a chroot at build stage. Packages from such repo would be updated automatically on schedule together with the rest of the packages in the chroot. When set to *true*, the repository is added to chroot temporary, only during the package builds. Apt cache for optional repos is also created at the time of the build. The base chroot would not have any packages from such repo after a package build is done.

### Environment variables

The module create pbuilder configuration file `/etc/pbuilderrc` , which has some customizeable behavior based on the environment variables in the user's shell configuration. The following variables are defined and can be set by a user:

* `USER_BUILDRESULT` - Location where you want the files resulting from the build to be deposited. Generally it is **mandatory** because if not set, the location would be a pbuilder's default `/var/cache/pbuilder/result`. You almost always want the build artifacts put somewhere in your home directory. The path can be relative or absolute. If relative path is specified, it is taken literally. If absolute path is given, then a codename of the release is added to the end of the path resulting in creation of subdirectories like *bookworm*, *bullseye*, *jammy* in it. The built packages are deposited in the respective subdirectory. To mimic the behavior of a pbuild script, set the *USER_BUILDRESULT* to `..`.
* `AUTO_DEBSIGN` - if set to "yes", pbuilder would attempt to automatically sign the packages you build. To take advantage of this feature you must have a gpg keychain with a signing key in your account, running gpg agent, and `DEBEMAIL` environment variable set to the name of the signing key.
* `DIST` - The OS release code name you are building a package for. Defaults to the OS and release of the build server itself. *Mandatory*, unless you are building for the same OS as the one running on a build server.
* `SHELL_ON_FAILURE` - drop to chroot shell if a package failed to build. Useful for troubleshooitng.
* `APT_USE_BUILT` - if you set `USER_BUILDRESULT` variable, necessary apt repository files would be added to it and the directory mounted into chroots at package build time. Thus you can have staged builds of the dependencies without a need to upload the intermediate packages to the remote repository.
* `<REPO_NAME>` - each *optional* repository you specify in a chroot description receives a corresponding variable in pbuilder configuration. When you set a variable corresponding to such repository to "yes", the repository is included in your package builds, but not added to the original base chroot. For instance, if you specified a custom repository `private_repo` in `chroots` hash, you can put `PRIVATE_REPO=yes` in your `~/.bashrc`, and this repository would be added to chroots during package builds.

Thus an example of what one may put in his `~/.bashrc`:

```
export AUTO_DEBSIGN="yes"
export PRIVATE_REPO="yes"
export APT_USE_BUILT="yes"
export USER_BUILDRESULT="${HOME}/result"
```

In this example pbuilder will automatically sign the packages, repository defined as `private_repo` in the *chroots* hash would be used during the package build, build artifacts would be deposited in a directory *result/<release_name>* within a home directory of a user and it would also be used as a local repo during the build, i.e. the packages already residing there would be made available during the build.

### Result directory structure

When an absolute path has been given as a value of `USER_BUILDRESULT` environment variable, pbuilder would create a subdirectory named as code name of a release you are building for. If you intend to use Debian suite names as release names, you may end up with both `stable` and `bookworm` result subdirectories. To avoid that, you may want to create symbolic links for suite names pointing to the release code name directories, like this:

```
result_directory
├── bookworm
├── bullseye
├── buster
├── focal
├── jammy
├── noble
├── oldoldstable -> buster
├── oldstable -> bullseye
├── sid
├── stable -> bookworm
├── testing -> trixie
├── trixie
└── unstable -> sid
```

### Building packages

The easiest way to build a package is to use pdebuild like this:

```
DIST=bookworm pdebuild
```

To almost mimic the behavior of *pbuild* wrapper, you can put something like this in your `~/.bashrc`:

```
function pbuild () {
        codename=$(dpkg-parsechangelog --show-field Distribution 2>/dev/null)
        if [ ! -z "$*" ]; then
                build_options="--debbuildopts ${*}"
        fi
        DIST="${codename}" pdebuild -- $build_options
}
```

Script detects your target release by reading the `changelog` file. You can start your build by running `pbuild` without any arguments or can specify additonal build options as arguments. Build options will be passed to `dpkg-buildpackage` as they are given as function arguments.

### Force chroot rebuilds

To force rebuild of a chroot without making any changes to its configuration in puppet, manually remove it first. Here is an example with a `bookworm` (Debian 12) chroot:

```bash
rm -rf /var/cache/pbuilder/base-bookworm.cow
rm /var/cache/pbuilder/base-stable.cow
```

If you would like apt cache and hooks to be rebuilt as well, then a couple more directories have to be removed as well:

```bash
rm -rf /var/cache/pbuilder/aptcache/bookworm
rm -rf /var/cache/pbuilder/hooks/bookworm
rm /var/cache/pbuilder/hooks/stable
```

After that initiate a puppet run. Puppet would recreate a chroot with the configuration as it is defined.

## Limitations

* No support for multiple architectures.
* Chroot gets rebuild when optional repo is removed.

## To do

* Feature requests welcome.

## References

* [dpkg-buildpackage(1)](https://manpages.debian.org/bookworm/dpkg-dev/dpkg-buildpackage.1.en.html)
* [dpkg-source(1)](https://manpages.debian.org/bookworm/dpkg-dev/dpkg-source.1.en.html)
* [dpkg-genchanges(1)](https://manpages.debian.org/bookworm/dpkg-dev/dpkg-genchanges.1.en.html)
* [pbuilder(8)](https://manpages.debian.org/bookworm/pbuilder/pbuilder.8.en.html)
* [pbuilderrc(5)](https://manpages.debian.org/bookworm/pbuilder/pbuilderrc.5.en.html)
* [pdebuild(1)](https://manpages.debian.org/bookworm/pbuilder/pdebuild.1.en.html)
* [debootstrap(8)](https://manpages.debian.org/bookworm/debootstrap/debootstrap.8.en.html)
* [cowbuilder(8)](https://manpages.debian.org/bookworm/cowbuilder/cowbuilder.8.en.html)

