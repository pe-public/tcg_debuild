# Changelog

All notable changes to this project will be documented in this file.

## Release 1.1.4

**Bug Fixes**

* Create debian suite symlinks in a hook directory. Otherwise builds
  against Debian suites do not pull optional repos.
* Export and use separate environment variable for bind mounts rahter
  then result directory because when building against Debian suites
  result directory retains the code name of a distro.

## Release 1.1.3

**Bug Fixes**

* Do not rebuild chroots when optional repo is added.

## Release 1.1.2

**Bug Fixes**

* Change in a set of packages in chroot triggers its rebuild.

## Release 1.1.1

**Features**

* Create symlinks for suite names allowing to trigger builds against
  Debian suite names.

**Bug Fixes**

* Removal of the repos from a chroot now triggers respective chroot rebuild.


## Release 1.1.0

**Features**

* Add automatic download of keys and keyrings from http(s) sources
  at build time directly from chroots.

## Release 1.0.0

**Features**

* Add automatic download of keys and keyrings from http(s) sources.

## Release 0.2.0

**Features**

* Add capability to automatically rebuild chroots if repository
  hooks have changed.
* Remove installation of sid repos on the build server. This is better
  done outside the module
* Automatically remove hooks that are not in use any longer.
* Optional repos are now added for pbuilder logins and execs.

**Bug Fixes**

* Environment variables are now properly passed to cowbuilder.

## Release 0.1.0

Initial release.

**Features**

* Create pbuilder base chroots for any Debian and Ubuntu release.
* Allow unprivileged users to build packages.
* Configuration completely configurable in hiera.
* Use debootstrap to import the keys into chroots.
* Use hooks to import extra repos in chroots.
* An option to embed repos into chroots or use them at the
  build time only.
* Use a single customizeable pbuilder configuration.
* Support for local repositories based on user's build results.
* An option to automatically open chroot shell on failed builds.
* An option to automatically sign packages.
* Obsolete pbuild script and use pdebuild directly.
* An option to delete chroots.

**Known Issues**

* Not thoroughly tested yet.
* No rspec testing code written.
