# @summary
#   Chroot definition
# 
type Tcg_debuild::Chroot = Struct[{
  'ensure'       => Enum['absent','present'],
  'distribution' => Enum['debian','ubuntu'],
  'mirror'       => Tcg_debuild::Repo,
  'suite'        => Optional[String[1]],
  'repos'        => Optional[Array[Tcg_debuild::Repo]],
  'packages'     => Optional[Array[String[1]]]
}]
