# @summary
#   Repository definition
#
type Tcg_debuild::Repo = Struct[{
  'repo_name'      => Pattern[/\A[a-zA-Z][a-zA-Z0-9_]*\Z/],
  'location'       => Stdlib::HTTPUrl,
  'priority'       => Optional[Integer[1,999]],
  'optional'       => Boolean,
  'components'     => Optional[String[1]],
  'release'        => Optional[String[1]],
  'release_suffix' => Optional[String[1]],
  'key'            => Optional[String[1]],
  'install_key'    => Optional[Boolean],
  'sources'        => Optional[Boolean],
}]
