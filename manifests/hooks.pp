# @summary
#   Deploy pbuioder hooks.
#
# @api private
#
# @param codename
#   Code name of the chroot.
#
# @param basepath
#   Path to the pbuilder cache.
#
# @param repos
#   An array of repository hashes.
#
define tcg_debuild::hooks (
  String $codename = $name,
  Stdlib::Unixpath $basepath = $tcg_debuild::basepath,
  Stdlib::Unixpath $keyring_dir = $tcg_debuild::keyring_dir,
  Array[Tcg_debuild::Repo] $repos = [],
) {
  # Opinionated directory locations relative to base pbuilder path
  $cow_dir = "${basepath}/base-${codename}.cow"
  $hook_dir = "${basepath}/hooks/${codename}"

  # Rebuild chroot when hook changes
  exec { "remove-rebuild-${codename}-chroot":
    path        => '/usr/sbin:/usr/bin:/bin',
    command     => "rm -rf ${cow_dir}",
    onlyif      => "test -d ${cow_dir}",
    refreshonly => true
  }

  # ADDITIONAL REPOS
  # Add a hook for each additional repository used in a chroot.
  # If a parameter "optional" in the repository hash has a value "false",
  # the hook is executed at build time leaving the repository in a chroot.
  # If "optiona" is set to "true", the repository is added prior to a
  # package build.
  #
  $repos.each |$repo| {
    # Multiple ways of specifying release in the list file:
    # * explicitly.
    # * adding a suffix to the distribution (like -security, -updates, etc.
    # * by default the same as distribution.
    #
    if 'release' in $repo {
      $release = $repo['release']
    } elsif 'release_suffix' in $repo {
      $release = "${codename}-${repo['release_suffix']}"
    } else {
      $release = $codename
    }

    # 'G's are incorporated in the chroot while 'D's are plugged in
    # on demand.
    if $repo['optional'] {
      $hook_prefixes = [ 'D', 'F' ]
      $rebuild_chroot = []
    } else {
      $hook_prefixes = [ 'G' ]
      $rebuild_chroot = Exec["remove-rebuild-${codename}-chroot"]
    }

    $hook_prefixes.each |$prefix| {
      file { "${hook_dir}/${prefix}03${repo['repo_name']}":
        content => template('tcg_debuild/hooks/extra_repos.erb'),
        mode    => '0755',
        notify  => $rebuild_chroot,
      }
    }
  }

  # DROP TO SHELL
  # If a build is unsuccessful, drop to shell in chroot for
  # further troubleshooting.
  #
  file { "${hook_dir}/C10shell":
    source => 'puppet:///modules/tcg_debuild/hooks/shell',
    mode   => '0755',
    notify => Exec["remove-rebuild-${codename}-chroot"]
  }

  # LOCAL REPO
  # Generate the necessary files to present the build result
  # directory as a local repository within a chroot. Useful for
  # handling intermediate packages.
  #
  file { ["${hook_dir}/D05local_sources", "${hook_dir}/F05local_sources"]:
    source => 'puppet:///modules/tcg_debuild/hooks/local_sources',
    mode   => '0755',
    notify => Exec["remove-rebuild-${codename}-chroot"]
  }

  # DO NOT REBUILD MAN DATABASE
  # Do not waste time on rebuilding man db in chroot.
  #
  file { "${hook_dir}/D80no-man-db-rebuild":
    source => 'puppet:///modules/tcg_debuild/hooks/no-man-db-rebuild',
    mode   => '0755',
    notify => Exec["remove-rebuild-${codename}-chroot"]
  }
}
