# @summary
#   Configurations for a Debian build server. Does *not* setup any chroot
#   environments.  For that you need to use tcg_debuild::chroot.
#
# @param cowbuild_group
#   Which group of users should be able to run cowbuilder with elevated
#   privileges.
#
# @param basepath
#   Location of pbuilder cache.
#
# @param chroots
#   Hash describing chroot environments to be created.
#   
#
class tcg_debuild (
  Enum['root', 'all'] $cowbuild_group = 'all',
  Stdlib::Unixpath $basepath = '/var/cache/pbuilder',
  Stdlib::Unixpath $keyring_dir = '/usr/share/keyrings',
  Hash[String, Tcg_debuild::Chroot] $chroots = {},
) {
  include tcg_debuild::packages

  ensure_packages(
    $tcg_debuild::packages::packages_to_install,
    {'ensure' => 'present'}
  )

  # Pin some packages to Debian backports.
  apt::pin { 'debian-build':
    ensure      => present,
    explanation => 'Pin some build tools on stable to backports.',
    packages    => $tcg_debuild::packages::pins,
    release     => "${facts['os']['distro']['codename']}-backports",
    priority    => 991,
  }

  file { ["${basepath}/aptcache", "${basepath}/hooks"]:
    ensure => directory
  }

  file {
    '/etc/maven/settings.xml':
      source  => 'puppet:///modules/tcg_debuild/etc/maven/settings.xml',
      require => Package['maven'];
    '/srv/maven':
      ensure => directory,
      mode   => '2775';
  }

  # The pbuild scripts are in stanford-server-debuild
  package { 'stanford-server-debuild': ensure => installed }

  # Basic configuration for pbuilder. Note that pbuild and pbuilder
  # are not useful without chroot environments (see tcg_debuild::chroots).
  file { '/etc/pbuilderrc':
    content => template('tcg_debuild/etc/pbuilderrc.erb'),
    require => Package['pbuilder'],
  }

  # Create a /etc/sudoers.d for running cowbuilder
  case $cowbuild_group {
    'root': {
      $sudo_cowbuild_group = '%root'
      $sudo_cowbuild_text  = 'Members of the root group'
    }
    'all': {
      $sudo_cowbuild_group = 'ALL'
      $sudo_cowbuild_text  = 'All users'
    }
    default: {
      crit("unknown cowbuild_group '${cowbuild_group}'")
    }
  }

  # We need a list of optional repos to be added to sudo
  $optional_repos = $chroots.reduce([]) |$memo1,$release| {
    if 'repos' in $release[1] {
      $release[1]['repos'].reduce($memo1) |$memo2, $repo| {
        if $repo['optional'] and !($repo['repo_name'].upcase() in $memo2) {
          $memo2 + $repo['repo_name'].upcase()
        } else { $memo2 }
      }
    } else { $memo1 }
  }.join(' ')

  sudo::conf { 'cowbuilder':
    priority => 50,
    content  => [
      'Cmnd_Alias PBUILDER = /usr/sbin/pbuilder, /usr/bin/pdebuild, /usr/bin/debuild-pbuilder, /usr/sbin/cowbuilder',
      "Defaults!PBUILDER env_keep+=\"${optional_repos} BUILDRESULT DIST AUTO_DEBSIGN SHELL_ON_FAILURE APT_USE_BUILT USER_BUILDRESULT\"",
      "# ${sudo_cowbuild_text} can run cowbuilder (needed for building Debian packages)",
      "${sudo_cowbuild_group} ALL = NOPASSWD:SETENV: PBUILDER",
    ],
  }

  # Set up the chroots.
  $chroots.each |$chroot, $params| {
    tcg_debuild::base { $chroot:
      * => $params,
    }
  }
}
