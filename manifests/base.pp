# @summary
#   Configure pbuilder roots.
#
# @param ensure
#   Create or remove chroot for a particular $distribution.
#
# @param codename
#   Normally the chroot uses $name as code name of the Debian or Ubuntu
#   release, which should be used for chroot creation.
#
# @param basepath
#   Location of the pbuilder cache.
#
# @param distribution
#   Suported Linux distributions, for now Debian and Ubuntu.
#
# @param suite
#   Debian suites (unstable, stable, etc.). Creates an alias with
#   the name of the suite pointing to a release chroot.
#
# @param repos
#   An array of hashes describing each repository to be used in
#   conjuction with this chroot.
#
# @param mirror
#   Main Debian or Ubuntu mirror for this chroot.
#
# @param keyring_dir
#   A directory to store the keyrings for chroots.
#
# @param packages
#   Extra packdges to be installed into chroot.
#
define tcg_debuild::base(
  Enum['present', 'absent'] $ensure     = present,
  Stdlib::Unixpath $basepath            = $tcg_debuild::basepath,
  Stdlib::Unixpath $keyring_dir         = $tcg_debuild::keyring_dir,
  String $codename                      = $name,
  Enum['debian','ubuntu'] $distribution = 'debian',
  Optional[String] $suite               = undef,
  Tcg_debuild::Repo $mirror             = {},
  Array[Tcg_debuild::Repo] $repos       = [],
  Array[String] $packages               = [],
) {
  # Opinionated directory locations relative to base pbuilder path
  $cow_dir = "${basepath}/base-${codename}.cow"
  $cache_dir = "${basepath}/aptcache/${codename}"
  $hook_dir = "${basepath}/hooks/${codename}"

  if ($ensure == 'present') {
    # create directories for an apt cache and hooks
    file { $cache_dir:
      ensure => directory
    }

    file { $hook_dir:
      ensure  => directory,
      purge   => true,
      recurse => true
    }

    # Extra packages to install, if any
    $extra_packages = $packages.empty ? {
      true    => '',
      default => "--extrapackages \"${packages.join(' ')}\""
    }

    # Store a list of installed packages in this base in a fact
    # to detect changes and force chroot rebuilds.
    #
    file { "pkgs-${codename}-fact":
      ensure  => file,
      path    => "/etc/puppetlabs/facter/facts.d/pkgs_${codename}.yaml",
      content => stdlib::to_yaml({"pkgs_${codename}" => sort($packages)}),
    }

    # Deploy pbuilder hooks and rebuild chroots on changes
    tcg_debuild::hooks { $codename:
      repos     => $repos,
      subscribe => [ File[$hook_dir], File["pkgs-${codename}-fact"] ],
    }

    # Collect all required keyrings, ignore repetitions.
    $extra_keyrings = $repos.reduce('') |$m, $repo| {

      # Install keyrings on the build server if install_key is requested.
      # Otherwise they would be installed with a hook.
      #
      if !('install_key' in $repo) or $repo['install_key'] {
        # Download a repo key
        if $repo['key'] =~ Stdlib::HTTPUrl {

          # Keyring file name is a name of a repo with a keyring suffix
          $filename = "${regsubst($repo['repo_name'], /_/, '-', 'G')}-keyring.gpg"
          $keyring = "${keyring_dir}/${filename}"

          if $repo['key'] =~ /\.gpg$/ {
            # Repo key is in keybox format
            $extract = false
            $path = $keyring
          } else {
            # keyring in a text format
            $extract = true
            $path = "/tmp/${filename}"
          }

          unless defined(Archive[$keyring]) {
            archive { $keyring:
              ensure          => $ensure,
              path            => $path,
              source          => $repo['key'],
              extract         => $extract,
              extract_path    => $keyring_dir,
              extract_command => "gpg --dearmor < %s > ${filename}",
              creates         => $keyring,
              cleanup         => true,
            }
          }
        } else {
          # Keyring already on the server
          $keyring = "${keyring_dir}/${repo['key']}"
        }

        unless $keyring in $m {
          "${m} --keyring=${keyring}"
        } else { $m }
      } else { $m }
    }

    # cowbuilder command to create a pbuilder base chroot
    $create_command = @("CREATE_CMD"/L)
    /usr/sbin/cowbuilder --create \
    --mirror ${mirror['location']} \
    --distribution ${codename} \
    --components "${mirror['components']}" \
    --hookdir "${hook_dir}/" \
    --aptcache "${cache_dir}" \
    --basepath ${cow_dir} \
    ${extra_packages} \
    --debootstrapopts --variant=buildd \
    --keyring=${keyring_dir}/${mirror['key']} \
    ${extra_keyrings}
    | CREATE_CMD

    # If this is an Ubuntu distribution, link the distribution name to
    # "gutsy".
    if $distribution == 'ubuntu' {
      file { "/usr/share/debootstrap/scripts/${codename}":
        ensure => 'link',
        target => '/usr/share/debootstrap/scripts/gutsy',
      }
    }

    exec { "cowbuilder-create-${codename}":
      path        => '/usr/sbin:/usr/bin:/bin',
      environment => [ "DIST=${codename}" ],
      command     => $create_command,
      creates     => $cow_dir,
      require     => [ Tcg_debuild::Hooks[$codename], File[$cache_dir] ],
    }

    if $suite =~ NotUndef {
      file { "${basepath}/base-${suite}.cow":
        ensure => link,
        target => $cow_dir
      }

      file { "${basepath}/hooks/${suite}":
        ensure => link,
        target => $hook_dir
      }
    }

    $ignore_messages = [
      'W: /root/.pbuilderrc does not exist',
      'W: --override-config is not set',
    ].join('|')

    # cowbuilder command to update a pbuilder base chroot
    $update_command = @("UPDATE_CMD"/L)
    /usr/sbin/cowbuilder update \
    --no-cowdancer-update 2>&1 >/dev/null | egrep -v "${ignore_messages}"
    | UPDATE_CMD

  } else {
    # delete a pbuilder base chroot as well as
    # apt caches and hooks associated with it.
    exec { "delete-${codename}-cache":
      path    => '/usr/sbin:/usr/bin:/bin',
      command => "rm -rf ${cow_dir}; rm -rf ${cache_dir}; rm -rf ${hook_dir}",
      onlyif  => "test -d ${cow_dir}",
    }

    $update_command = '/bin/true'
  }

  # Update chroot on schedule
  cron::job::multiple { "cowbuilder-update-${codename}":
    ensure      => $ensure,
    jobs        => [
      {
        description => "Update '${codename}' cowbuilder chroot on schedule.",
        command     => $update_command,
        minute      => "${fqdn_rand(59, $title)}",
        hour        => '2',
        user        => 'root'
      },
      {
        description => "Update '${codename}' cowbuilder chroot on boot.",
        command     => $update_command,
        special     => 'reboot',
        user        => 'root'
      },
    ],
    environment => [
      "DIST=${codename}",
      'PATH="/usr/bin:/usr/sbin"',
    ],
  }

}
